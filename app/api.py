from app import app, db # importing from app from the package app
from flask_restplus import Resource, Api, fields
from app.models import temp_logging
import datetime
from pytz import timezone

api = Api(app)

temp_reading = api.model("Temp logging", {
    'sensor': fields.String('The sensor'),
    'temp': fields.Float('temperature'),
    'logged_at': fields.DateTime
})

@api.route('/temp')
class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

    @api.expect(temp_reading)
    def post(self):
        save_new_temp_reading(data=api.payload)
        return {'Temp reading':api.payload}
        

def save_new_temp_reading(data):
    strdate = data['logged_at']
    dt = datetime.datetime.strptime(strdate, '%Y-%m-%dT%H:%M:%S.%f%z')
    local_dt = dt.astimezone(timezone('Europe/Oslo'))

    print("datetime {}".format(dt))
    print("local time {}".format(local_dt))
    reading = temp_logging(
        sensor=data['sensor'],
        temp=data['temp'],
        logged_at=local_dt
    )
    db.session.add(reading)
    db.session.commit()
    return reading
