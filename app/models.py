
from datetime import datetime
from app import db


class temp_logging(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sensor = db.Column(db.String(80), nullable=False)
    temp = db.Column(db.Float, nullable=False)
    logged_at = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return '<Post %r>' % self.sensor