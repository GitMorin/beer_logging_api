from flask import Flask
from flask_sqlalchemy import SQLAlchemy 


app = Flask(__name__) # Create flask app
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///beer.db'
db = SQLAlchemy(app)


from app import api
from app import main