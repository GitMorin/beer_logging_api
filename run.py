from app import app # Importing the app object from the init file

# Entry point in root directory

if __name__ == "__main__":
  app.run()